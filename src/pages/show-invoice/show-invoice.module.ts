import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowInvoicePage } from './show-invoice';

@NgModule({
  declarations: [
    ShowInvoicePage,
  ],
  imports: [
    IonicPageModule.forChild(ShowInvoicePage),
  ],
})
export class ShowInvoicePageModule {}
