import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ShowInvoicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-show-invoice',
  templateUrl: 'show-invoice.html',
})
export class ShowInvoicePage {

  invoice: object;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.invoice = this.navParams.get('invoice');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShowInvoicePage');
  }

}
