import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {ShowInvoicePage} from "../show-invoice/show-invoice";

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})

export class ListPage {
  selectedItem: any;
  icons: string[];
  items: Array<{title: string, note: string, icon: string}>;
  invoices: Array<{invoiceNumber: string, customer: string, supplier: string, total: number, date: string}>

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    // If we navigated to this page, we will have an item available as a nav param
    this.selectedItem = navParams.get('item');
    let newInvoice = navParams.get('invoice');

    // Let's populate this page with some filler content for funzies
    this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
    'american-football', 'boat', 'bluetooth', 'build'];

    this.items = [];
    for (let i = 1; i < 11; i++) {
        this.items.push({
        title: 'Item ' + i,
        note: 'This is item #' + i,
        icon: this.icons[Math.floor(Math.random() * this.icons.length)]
      });
    }

    this.invoices = [
        {
            invoiceNumber: 'F001',
            customer: 'Customer Company',
            supplier: 'Supplier Company',
            total: 16.50,
            date: '01/11/18'
        },{
            invoiceNumber: 'F002',
            customer: 'Customer Company',
            supplier: 'Supplier Company',
            total: 32.00,
            date: '02/11/18'
        },{
            invoiceNumber: 'F003',
            customer: 'Customer Company',
            supplier: 'Supplier Company',
            total: 64.00,
            date: '03/11/18'
        }
    ];

    this.invoices.push(newInvoice);
  }

  itemTapped(event, invoice) {
    // That's right, we're pushing to ourselves!
    this.navCtrl.push(ShowInvoicePage, {
      invoice: invoice
    });
  }
}
