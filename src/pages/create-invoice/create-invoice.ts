import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ShowInvoicePage} from "../show-invoice/show-invoice";
import {ListPage} from "../list/list";

/**
 * Generated class for the CreateInvoicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-create-invoice',
  templateUrl: 'create-invoice.html',
})
export class CreateInvoicePage {

  invoice: Array<{invoiceNumber: string, customer: string, supplier: string, total: number, date: string}>

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.invoice = [];
  }

  createInvoice() {
    console.log(this.invoice);
    this.navCtrl.push(ListPage, {
        invoice: this.invoice
    });
  }
}
